{
  "personalInfo": {
    "Location": "Berlin, Germany",
    "Mail": "goesta.von.goetz@rwth-aachen.de",
    "Born": "1988 in Frankfurt am Main",
    "Nationality": "German"
  },
  "education": [
    {
      "title": "Master of Science",
      "subtitle": "Electrical Engineering, Information Technology and Computer Engineering",
      "duration": "04/2012 - 09/2014",
      "organization": "RWTH Aachen University",
      "location": "Aachen, Germany",
      "details": [
        "Major Field of Study: Biomedical Engineering",
        "Selected Courses: Digital Image Processing I&II, Biomedical Imaging",
        "Seminar „Personal Healthcare“: Development of an Eye Tracking System",
        "Master Thesis: Development of an Electrical Drive for a Total Artificial Heart Based on the Piston Engine Principle (carried out at the Institute of Electrical Machines and the Institute of Applied Medical Engineering, Cardiovascular Engineering Group of RWTH Aachen) - Mark: 1,3"
      ]
    },
    {
      "title": "Bachelor of Science",
      "subtitle": "Engineering and Business Administration",
      "duration": "10/2008 - 09/2011",
      "organization": "RWTH Aachen University",
      "location": "Aachen, Germany",
      "details": [
        "Major Field of Study: Electrical Power Engineering",
        "Bachelor Thesis: Design of an Electromagnetic Bearing for the Drive of a Ventricular Assist Device (carried out at the Institute of Electrical Machines of RWTH Aachen) - Mark: 1,0"
      ]
    },
    {
      "title": "High School",
      "subtitle": "Abitur",
      "duration": 2007,
      "organization": "Taunusgymnasium Königstein",
      "location": "Königstein im Taunus, Germany"
    }
  ],
  "occupationalHistory": [
    {
      "title": "Senior Professional Damage Business Development",
      "duration": "02/2019 - now",
      "organization": "Sixt SE",
      "department": "SXT Retina Lab GmbH",
      "link": "https://about.sixt.com/websites/sixt_cc/English/0/about-us.html",
      "details": [
        "Research on the applicability of neural networks for automatic damage detection",
        "Development of an image acquisition and processing pipeline, leveraging MQTT and CNNs",
        "Development of camera driver software for abovementioned pipeline",
        "Deployment of abovementioned pipeline as a containerized web application",
        "Engineering of artificial datasets for instance segmentation using the Blender Python API",
        "Development of a CNN-based solution for image classification (fastai, pytorch)",
        "Development of a CNN-based solution for instance segmentation (pytorch)",
        "Requirements engineering"
      ],
      "tags": [
        "neural networks",
        "machine learning",
        "data engineering",
        "computer vision",
        "object detection",
        "instance segmentation",
        "imaging",
        "edge computing",
        "light simulation",
        "blender API",
        "IoT",
        "MQTT",
        "Kafka",
        "GraphQL",
        "gRPC",
        "Docker",
        "docker-compose",
        "Gitlab CI/CD",
        "Kubernetes",
        "nvidia-docker2",
        "openCV",
        "pytorch",
        "fastai",
        "Mask-RCNN",
        "PyViz"
      ],
      "location": "Berlin, Germany"
    },
    {
      "title": "Software Engineer (Full Stack)",
      "duration": "04/2017 - 01/2019",
      "organization": "botspot GmbH",
      "link": "https://www.botspot.de/",
      "details": [
        "System architecture of 3D photogrammetry scanners",
        "Development of camera driver software for use in distributed sensor networks (C++, boost, SWIG, Python)",
        "Design of APIs to facilitate customer specific scanning workflows (openAPI, flask)",
        "Requirements management for a large range of applications",
        "3D reconstruction of scans (RealityCapture SDK)",
        "Design of electronics for highly synchronous imaging (KiCAD)"
      ],
      "tags": [
        "3D reconstruction",
        "imaging",
        "boost",
        "boost::msm",
        "boost::mpl",
        "C++",
        "Python",
        "cmake",
        "MQTT",
        "IoT",
        "Docker",
        "http",
        "flask",
        "electron",
        "node",
        "vue.js",
        "nginx",
        "TI RTOS (MSP432)",
        "embedded",
        "microservice architecture",
        "edge computing"
      ],
      "location": "Berlin, Germany"
    },
    {
      "title": "Doctoral Researcher",
      "duration": "10/2015 - 03/2017",
      "organization": "Forschungszentrum Jülich GmbH - Electronic Systems (ZEA-2)",
      "department": "Team Modelling and Algorithms",
      "link": "http://www.fz-juelich.de/zea/zea-2/EN/Home/home_node.html",
      "details": [
        "Framework project: Software for high-resolution micro-computed tomography at Forschungszentrum Jülich (internal project partners: ZEA-1, IEK-2)",
        "Development of fast iterative reconstruction algorithms for x-ray computed tomography",
        "Parallelization and pipelined implementation of high-accuracy projection algorithms on general purpose GPUs (CUDA)",
        "Data post-processing and vizualization (vtk)"
      ],
      "tags": [
        "3D reconstruction",
        "imaging",
        "HPC",
        "parallel computing",
        "CUDA",
        "C++",
        "vtk",
        "cmake",
        "nsight"
      ],
      "location": "Jülich, Germany",
      "publications": [
        {
          "author": "von Götz et al.",
          "title": "Fast Iterative Reconstruction: A Comparison of Hardware-Accelerated Projection Methods",
          "journal": "Proceedings of the 7th Conference on Industrial Computed Tomography",
          "location": "Leuven",
          "year": 2017,
          "link": "https://www.ndt.net/events/iCT2017/app/content/Paper/32_VonGotz.pdf"
        }
      ]
    },
    {
      "title": "Freelance Developer",
      "duration": "03/2015 - 09/2015",
      "organization": "Velocity Aachen GmbH",
      "link": "https://velocity-aachen.de/",
      "location": "Aachen, Germany",
      "details": [
        "Design of controller hardware for integration into a rental E-Bike (KiCAD)",
        "Development of emedded communication software for handling communication between battery, motor and charging stations"
      ],
      "tags": [
        "KiCAD",
        "embedded",
        "Atmel AVR",
        "CAN"
      ]
    },
    {
      "title": "Internship",
      "duration": "10/2013 - 12/2013",
      "organization": "ICETLab, The Prince Charles Hospital",
      "link": "http://icetlab.com/",
      "location": "Brisbane, Australia",
      "details": [
        "Design, construction and programming of a controller for the clinically available ventricular assist device HVAD by Heartware Inc.",
        "Building test rigs for cardiovascular technology",
        "Setup of a CAN-network for communication with a dSPACE system",
        "Supervision of a student with the construction of electrical circuits"
      ],
      "tags": [
        "Texas Instruments",
        "DRV8312",
        "Piccolo MCU",
        "embedded",
        "motion control",
        "electrical machines",
        "CAN",
        "dSPACE"
      ]
    },
    {
      "title": "Internship",
      "duration": "09/2011 - 01/2012",
      "organization": "Korea Electrotechnology Resarch Institute",
      "link": "https://www.keri.re.kr/html/en/",
      "location": "Changwon, South Korea",
      "details": [
        "Carrying out a benchmarking study",
        "Modelling and calculation of electrical machines with finite elements method (Ansoft Maxwell)",
        "CAD, creation of renderings, animations and technical drawings (Autodesk Inventor)",
        "Participation in an intensive seminar on electrical machines and mechatronic devices held by Prof. Dr. E. Lomonova (TU Eindhoven)"
      ],
      "tags": [
        "Autodesk Inventor",
        "CAD",
        "finite elements",
        "electrical machines"
      ]
    },
    {
      "title": "Student Researcher",
      "duration": "12/2009 - 07/2011 & 03/2012 - 12/2012",
      "organization": "Institute of Electrical Machines, RWTH Aachen",
      "location": "Aachen, Germany",
      "link": "https://www.iem.rwth-aachen.de/aw/cms/IEM/Startseite/~rzv/home/?lang=en",
      "details": [
        "Modelling and calculation of problems with finite elements method on the RWTH HPC Cluster (ANSYS, PETSc, MOOSE and vtk)",
        "Research and writing of texts for current lectures (Components and Installa- tions for the Supply of Electricity)"
      ],
      "tags": [
        "HPC",
        "simulation",
        "ANSYS",
        "CAD",
        "finite elements",
        "electrical machines"
      ]
    }
  ],
  "scholarshipsAndVoluntary": [
    {
      "title": "Institute Delegate",
      "duration": "10/2015 - 03/2017",
      "organization": "DocTeam (special interest group for PhDs at FZ Jülich)",
      "details": [
        "Representing the interests of ZEA-2 doctoral researchers towards FZ Jülich"
      ]
    },
    {
      "title": "Doctoral Fellow",
      "duration": "10/2015 - 03/2017",
      "organization": "HiTEC Graduate School of the Helmholtz Society",
      "details": [
        "Participation in various courses concerning good scientific practice"
      ]
    },
    {
      "title": "Active Membership",
      "duration": "05/2009 - 09/2010",
      "organization": "Engineers without Boarders International, Aachen Branch",
      "details": [
        "Organization of lectures on technical aspects of development cooperation"
      ]
    },
    {
      "title": "Scholarship",
      "duration": "10/2013 - 12/2013",
      "organization": "PROMOS-Scholarship of the German Academic Exchange Service",
      "details": [
        "Sponsorship of an internship abroad at the ICETLab, Brisbane, Australia"
      ]
    }

  ],
  "skillSets": [
    {
      "title": "Programming Languages",
      "skills": [
        {
          "name": "Python",
          "level": 3
        },
        {
          "name": "C",
          "level": 3
        },
        {
          "name": "C++",
          "level": 2
        },
        {
          "name": "JavaScript",
          "level": 3
        },
        {
          "name": "Matlab",
          "level": 2
        },
        {
          "name": "Java",
          "level": 2
        }
      ]
    },
    {
      "title": "Building, Packaging",
      "skills": [
        {
          "name": "CMake"
        },
        {
          "name": "make"
        },
        {
          "name": "SWIG"
        },
        {
          "name": "yarn/npm"
        },
        {
          "name": "Python setuptools"
        },
        {
          "name": "Vendor-specific toolchains"
        }
      ]
    },
    {
      "title": "Profiling",
      "skills": [
        {
          "name": "valgrind"
        },
        {
          "name": "nsight"
        }
      ]
    },
    {
      "title": "Version Control",
      "skills": [
        {
          "name": "git"
        },
        {
          "name": "integration manager workflow"
        },
        {
          "name": "GitLab"
        },
        {
          "name": "Github"
        }
      ]
    },
    {
      "title": "Documentation",
      "skills": [
        {
          "name": "doxygen"
        },
        {
          "name": "OpenAPI 3"
        },
        {
          "name": "vuepress"
        }
      ]
    },
    {
      "title": "HPC, AI, Computer Vision",
      "skills": [
        {
          "name": "fastai"
        },
        {
          "name": "pytorch"
        },
        {
          "name": "CUDA"
        },
        {
          "name": "numpy"
        },
        {
          "name": "OpenCV"
        },
        {
          "name": "openMP"
        }
      ]
    },
    {
      "title": "Graphics, Visualization",
      "skills": [
        {
          "name": "vtk"
        },
        {
          "name": "PyViz"
        },
        {
          "name": "bokeh"
        },
        {
          "name": "blender"
        }
      ]
    },
    {
      "title": "Frontend",
      "skills": [
        {
          "name": "vue.js"
        },
        {
          "name": "electron"
        },
        {
          "name": "angular"
        },
        {
          "name": "Qt/PySide2"
        },
        {
          "name": "bulma"
        }
      ]
    },
    {
      "title": "Backend",
      "skills": [
        {
          "name": "flask"
        },
        {
          "name": "starlette"
        },
        {
          "name": "postgresql"
        },
        {
          "name": "SQLAlchemy"
        },
        {
          "name": "GraphQL"
        },
        {
          "name": "graphene"
        },
        {
          "name": "Kafka"
        },
        {
          "name": "gRPC"
        }
      ]
    },
    {
      "title": "Protocols",
      "skills": [
        {
          "name": "http"
        },
        {
          "name": "MQTT"
        },
        {
          "name": "websockets"
        },
        {
          "name": "ØMQ"
        },
        {
          "name": "CAN"
        }
      ]
    },
    {
      "title": "DevOps",
      "skills": [
        {
          "name": "docker"
        },
        {
          "name": "docker-compose"
        },
        {
          "name": "ansible"
        },
        {
          "name": "Gitlab CI/CD"
        },
        {
          "name": "heroku"
        },
        {
          "name": "kubernetes"
        },
        {
          "name": "helm"
        }
      ]
    },
    {
      "title": "Collaboration",
      "skills": [
        {
          "name": "GitLab"
        },
        {
          "name": "Slack"
        },
        {
          "name": "Trello"
        },
        {
          "name": "MS Office"
        }
      ]
    },
    {
      "title": "Spoken Languages",
      "skills": [
        {
          "name": "English",
          "level": "profound"
        },
        {
          "name": "Spanish",
          "level": "basic"
        },
        {
          "name": "French",
          "level": "very basic"
        }
      ]
    }
  ],
  "aboutMyself": [
    {
      "title": "Interests",
      "skills": [
        {
          "name": "A/D imaging"
        },
        {
          "name": "image processing"
        },
        {
          "name": "computer graphics"
        },
        {
          "name": "design"
        },
        {
          "name": "cooking"
        },
        {
          "name": "fermentation"
        }
      ]
    }
  ]
}
