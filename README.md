# cv

This is my CV, which I have built using [vue.js](https://vuejs.org/), [bulma](https://bulma.io/) and [fontawesome](https://fontawesome.com/). It uses [Gitlab CI/CD](https://docs.gitlab.com/ce/ci/) to deploy to [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/), following the instructions found [here](https://cli.vuejs.org/guide/deployment.html#gitlab-pages).

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
